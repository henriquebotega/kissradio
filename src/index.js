import React, { Component } from 'react';
import { WebView } from 'react-native-webview';

export default class App extends Component {
  render() {
    return (
      <WebView style={{ flex: 1 }} source={{ uri: 'http://player.kissradio.ca/?skipap=1' }} />
    );
  }
}
